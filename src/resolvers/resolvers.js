// const UserType = require('../utils/enums/UserType')
const shortid = require('shortid')
const db = require('../utils/db')
const { request } = require('graphql-request')

const accounts = []

module.exports = {
  Query: {
    getAccounts: (_parent, _args, _context, _info) => db.collection('accounts').find({}).toArray(),
    getAccount: (_parent, args, _context, _info) => {
      const accountId = args._id
      return db.collection('accounts').findOne({ _id: accountId })

    },
  },
  Mutation: {
    createAccount: async (_parent, args, _context, _info) => {
      const { userInput } = args

      userInput._id = shortid.generate()

      switch (userInput.type) {
        case 'PRODUCER':
          userInput.taxId = ""
          userInput.producerProfile = {}
          break;
        case 'CONSUMER':
          userInput.preferedPickupStation = {}
          userInput.preferedPaymentMethod = ""
          break;
        default:
          break;
      }

      const upperCaseType = userInput.type.toUpperCase();
      const authQuery = `
      mutation createAuthAccount{
        createAuthAccount(accountInputWithId: {_id: "${userInput._id}", email: "${userInput.email}", password: "${userInput.password}", accountType: ${upperCaseType}}){
          _id
          email
        }
      }
      `
      const shoppingCartQuery = `
      mutation createShoppingCart{
        createShoppingCart(editShoppingCartInput: { consumerId: "${userInput._id}"}){
          _id
          consumerId
          products{productId}
        }
      }
      `
      const shoppingCart = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', shoppingCartQuery).then(data => {
        return data.createShoppingCart
      })

      const userResult = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', authQuery).then(async data => {
        const { _id, email } = data.createAuthAccount

        if (!_id) return null
        delete userInput.password
        const existingAccount = await db.collection('accounts').findOne({ $or: [{ _id: _id }, { email: email }] })
        if (existingAccount) {
          return existingAccount
        } else {
          const createdAccount = await await db.collection('accounts').insertOne(userInput)
          return createdAccount.ops[0]
        }

      })
      return userResult
    },
    deleteAccount: async (_parent, args, context, _info) => {
      const accountId = args._id
      const deleteAuthAccounQuery = `
      mutation deleteAuthAccount{
        deleteAuthAccount(_id: "${accountId}"){
          _id
        }
      }
      `
      const deleteShoppingCartQuery = `
      mutation removeShoppingCart{
        removeShoppingCart(editShoppingCartInput: { consumerId: "${accountId}"}){
          _id
        }
      }
      `
      const deleteProductsByConsumerQuery = `
      mutation deleteProductsByProducer{
        deleteProductsByProducer(producerID: "${accountId}"){
          _id
        }
      }
      `
      const deleteSpecificTemplatesQuery = `
      mutation deleteSpecificTemplates{
        deleteSpecificTemplates(producerId: "${accountId}"){
          _id
        }
      }
      `
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == accountId || user.accountType == 'ADMIN') {
        await db.collection('accounts').deleteOne({ _id: accountId }, async (err, result) => {
          if (err) {
            return err
          } else {
            const deleteAuth = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', deleteAuthAccounQuery).then(data => {
              return data.deleteAuthAccount
            })
            const deleteShoppingCart = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', deleteShoppingCartQuery).then(data => {
              return data.removeShoppingCart
            })

            if (user.accountType == 'PRODUCER' || user.accountType == 'ADMIN') {
              const deleteProducts = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', deleteProductsByConsumerQuery).then(data => {
                return data.deleteProductsByProducer
              })
              const deleteTemplates = await request(process.env.SERVICE_GATEWAY_URL || 'http://localhost:4000/graphql', deleteSpecificTemplatesQuery).then(data => {
                return data.deleteSpecificTemplates
              })
            }
            console.log(`Removed document with accountID: ${accountId}`)
            return result
          }
        })
      } else {
        return null
      }
    },

    /**
     * TODO: Prevent edit of accountType (e.g. Admin...)
     */
    editAccount: async (_parent, args, context, _info) => {
      const { _id, userInput } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      console.log(user.accountType);
      if (user.id == _id || user.accountType == 'ADMIN') {
        await db.collection('accounts').replaceOne({ _id: _id }, userInput)
        return db.collection('accounts').findOne({ _id: _id })
      } else {
        return null
      }
    },

    editProducerProfile: async (_parent, args, context, _info) => {

      /**
       * TODO:
       * - implement q-media interaction
       */
      const { _id, userInput } = args
      const { req } = context
      const user = JSON.parse(req.headers.user)
      if (user.id == accountId || user.accountType == 'ADMIN') {
        if (userInput.type == "PRODUCER") {
          await db.collection('accounts').updateOne({ _id: _id }, { $set: { producerProfile: userInput } })
        }
        return db.collection('accounts').findOne({ _id: _id })
      } else {
        return null
      }

    },
    /**
     * probably obsolete, we need to decide we need to divide the account creation process and editing into producer and consumer
     */
    /*     
        createConsumer: (_parent, args, context, _info) => {
          const { consumerInput } = args
          accounts.push(consumerInput)
    
          return accounts[accounts.length - 1]
        },
        editConsumer: (_parent, args, context, _info) => {
          const { consumerInput } = args
          accounts.push(consumerInput)
          return accounts[accounts.length - 1]
        },
        createProducer: (_parent, args, context, _info) => {
          const { producerInput } = args
          accounts.push(producerInput)
    
          return accounts[accounts.length - 1]
        },
        editProducer: (_parent, args, context, _info) => {
          const { producerInput } = args
          accounts.push(producerInput)
          return accounts[accounts.length - 1]
        }, */
  },
  Account: {
    __resolveType: (account) => {
      switch (account.type) {
        case 'CONSUMER': return 'Consumer'
        case 'PRODUCER': return 'Producer'
        case 'ADMIN': return 'Admin'
        default: throw new Error('User could not be identified.')
      }
    },
    __resolveReference(reference) {
      return accounts[accounts.length - 1]
    },
  },
  Consumer: {
    __resolveReference(reference) {
      return accounts[accounts.length - 1]
    },
    purchases: ({ purchasedProductIds }) => purchasedProductIds
      .map(id => ({ __typename: 'Product', id })),
  },
  Producer: {
    __resolveReference(reference) {
      return accounts[accounts.length - 1]
    },
  },
  Admin: {
    __resolveReference(reference) {
      return accounts[accounts.length - 1]
    },
  },
  UserType: {
    PRODUCER: 'producer',
    CONSUMER: 'consumer',
    ADMIN: 'admin',
  },
}
